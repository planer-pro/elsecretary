﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ElSecretary.Startup))]
namespace ElSecretary
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
