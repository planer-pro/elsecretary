﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ElSecretary.Models;
using Microsoft.AspNet.Identity;

namespace ElSecretary.Controllers
{
    public class ForumController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: Forum
        public ActionResult Index(int? id)
        {
            List<ForumMessage> forumMessages = db.ForumMessages.ToList();
            List<ForumMessage> forumMessagesOut = new List<ForumMessage>();
            foreach (var item in forumMessages)
            {
                if (item.ItemCatalogId == id)
                {
                    forumMessagesOut.Add(item);
                }
            }
            ViewBag.isAdmin = User.Identity.GetUserName();
            ViewBag.idDevice = id;

            return View(forumMessagesOut);
        }

        [Authorize]
        [HttpPost]
        public ActionResult NewMessage(ForumMessage forumModel, int? idDevice)
        {

            forumModel.Owner = User.Identity.GetUserName();
            forumModel.DateTime = DateTime.Now;
            forumModel.ItemCatalogId = idDevice;

            db.ForumMessages.Add(forumModel);
            db.SaveChanges();

            Response.Redirect(Request.UrlReferrer.ToString());
            return null;
        }

        [Authorize(Users = "admin@els.com")]
        public ActionResult DeleteMessage(int? id)
        {
            ForumMessage message = db.ForumMessages.Find(id);
            if (message == null)
                return HttpNotFound();

            db.ForumMessages.Remove(message);
            db.SaveChanges();
            
            Response.Redirect(Request.UrlReferrer.ToString());

            return null;
        }
    }
}