﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ElSecretary.Models;
using Microsoft.AspNet.Identity;
using ImageResizer;

namespace ElSecretary.Controllers
{
    public class CatalogController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {

            if (User.Identity.GetUserName() == "admin@els.com")
            {
                List<ItemCatalog> cropTextToDescriptAdmin = db.ItemCatalogs.ToList();
                foreach (var item in cropTextToDescriptAdmin)
                {
                    if (item.Description.Length > 50)
                        item.Description = item.Description.Substring(0, 50) + "...";
                }
                return View("IndexAdmin", cropTextToDescriptAdmin);
                
            }
            else if (User.Identity.GetUserName() != "")
            {
                List<ItemCatalog> cropTextToDescriptUser = db.ItemCatalogs.ToList();
                foreach (var item in cropTextToDescriptUser)
                {
                    if (item.Description.Length > 50)
                        item.Description = item.Description.Substring(0, 50) + "...";
                }
                return View("IndexRegistred", cropTextToDescriptUser);
            }

            List<ItemCatalog> cropTextToDescriptGuest = db.ItemCatalogs.ToList();
            foreach (var item in cropTextToDescriptGuest)
            {
                if (item.Description.Length > 50)
                    item.Description = item.Description.Substring(0, 50) + "...";
            }
            return View(cropTextToDescriptGuest);
        }

        // GET: Catalog/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemCatalog itemCatalog = db.ItemCatalogs.Find(id);
            if (itemCatalog == null)
            {
                return HttpNotFound();
            }
            return View(itemCatalog);
        }

        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ItemCatalog itemCatalog, HttpPostedFileBase uploadImage)
        {
            if (ModelState.IsValid)
            {
                if (uploadImage != null)
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        uploadImage.InputStream.Seek(0, System.IO.SeekOrigin.Begin);

                        ImageBuilder.Current.Build(
                            new ImageJob(uploadImage.InputStream, ms, new Instructions("maxwidth=200&maxheight=200"),
                                false,
                                false));
                        itemCatalog.Image = ms.ToArray();
                    }
                }
                db.ItemCatalogs.Add(itemCatalog);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(itemCatalog);
        }

        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemCatalog itemCatalog = db.ItemCatalogs.Find(id);
            if (itemCatalog == null)
            {
                return HttpNotFound();
            }
            return View(itemCatalog);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ItemCatalog itemCatalog, HttpPostedFileBase uploadImage)
        {
            if (ModelState.IsValid)
            {
                if (uploadImage != null)
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        uploadImage.InputStream.Seek(0, System.IO.SeekOrigin.Begin);

                        ImageBuilder.Current.Build(
                            new ImageJob(uploadImage.InputStream, ms, new Instructions("maxwidth=280&maxheight=280"),
                                false,
                                false));
                        itemCatalog.Image = ms.ToArray();
                    }
                }
                db.Entry(itemCatalog).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(itemCatalog);
        }

        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemCatalog itemCatalog = db.ItemCatalogs.Find(id);
            if (itemCatalog == null)
            {
                return HttpNotFound();
            }
            return View(itemCatalog);
        }

        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ItemCatalog itemCatalog = db.ItemCatalogs.Find(id);
            db.ItemCatalogs.Remove(itemCatalog);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
