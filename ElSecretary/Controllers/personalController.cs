﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ElSecretary.Models;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;

namespace ElSecretary.Controllers
{
    [Authorize]
    public class PersonalController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: personal
        public ActionResult Index()
        {
            List<ItemPersonal> itemPersonals = new List<ItemPersonal>();

            foreach (var item in db.ItemPersonals.Include(x=>x.ItemCatalog))
            {
                if (item.Owner == User.Identity.GetUserName())
                    itemPersonals.Add(item);
            }

            return View(itemPersonals);
        }

        // GET: personal/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemPersonal itemPersonal = db.ItemPersonals.Find(id);
            if (itemPersonal == null || itemPersonal.Owner != User.Identity.GetUserName())
            {
                return HttpNotFound();
            }
            return View(itemPersonal);
        }

        // GET: personal/Create
        public ActionResult Create()
        {
            List<ItemCatalog> items = db.ItemCatalogs.ToList();
            ViewBag.itemsCatalog = items;

            return View();
        }

        // POST: personal/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ItemPersonal itemPersonal, int? itemSelector)
        {
            if(itemSelector==null)
                ModelState.AddModelError("","Please select device from list");

            if (ModelState.IsValid)
            {

                itemPersonal.Owner = User.Identity.GetUserName();

               

                itemPersonal.ItemCatalogId = itemSelector;

                db.ItemPersonals.Add(itemPersonal);
                db.SaveChanges();
                return RedirectToAction("Index");

            }

            List<ItemCatalog> items = db.ItemCatalogs.ToList();
            ViewBag.itemsCatalog = items;

            return View(itemPersonal);
        }

        // GET: personal/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemPersonal itemPersonal = db.ItemPersonals.Find(id);
            if (itemPersonal == null || itemPersonal.Owner != User.Identity.GetUserName())
            {
                return HttpNotFound();
            }
            return View(itemPersonal);
        }

        // POST: personal/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ItemPersonal itemPersonal)
        {
            if (ModelState.IsValid)
            {
                //todo сделать проверку овнера 
                itemPersonal.Owner = User.Identity.GetUserName();

                db.Entry(itemPersonal).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(itemPersonal);
        }

        // GET: personal/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemPersonal itemPersonal = db.ItemPersonals.Find(id);
            if (itemPersonal == null || itemPersonal.Owner != User.Identity.GetUserName())
            {
                return HttpNotFound();
            }
            return View(itemPersonal);
        }

        // POST: personal/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {

            ItemPersonal itemPersonal = db.ItemPersonals.Find(id);
            if (itemPersonal.Owner == User.Identity.GetUserName())
            {
                db.ItemPersonals.Remove(itemPersonal);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return HttpNotFound();

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
