﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElSecretary.Models
{
    public class ItemCatalog
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public byte[] Image { get; set; }
        public int AverageCost { get; set; }
        public int Raiting { get; set; }
     
    }
}