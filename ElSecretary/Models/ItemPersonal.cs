﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ElSecretary.Models
{
    public class ItemPersonal
    {

        public int Id { get; set; }
        public string Note { get; set; }
        public int Cost { get; set; }
        public bool ShareToCatalog { get; set; }
        public int Quantity { get; set; }
        public int Quality { get; set; }
        public string Owner { get; set; }

        public ItemCatalog ItemCatalog { get; set; }
        public int? ItemCatalogId { get; set; }
    }
}