﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElSecretary.Models
{
    public class ForumMessage
    {
        public int Id { get; set; }
        public string Owner { get; set; }
        public int? ItemCatalogId { get; set; }
        public ItemCatalog ItemCatalog { get; set; }
        public string Message { get; set; }
        public DateTime DateTime { get; set; }
    }
}